export const dataForLocalStorage = [
	{ 	
		id: 1,
		month: "January",
		income: 954.45,
		expenses: 712.35
	},
	{ 	
		id: 2,
		month: "February",
		income: 1054.45,
		expenses: 840.35
	},
	{ 	
		id: 3,
		month: "March",
		income: 854.45,
		expenses: 940.35
	},
	{ 	
		id: 4,
		month: "April",
		income: 1012.45,
		expenses: 1350.35
	},
	{ 	
		id: 5,
		month: "May",
		income: 1245.45,
		expenses: 659.35
	},
	{ 	
		id: 6,
		month: "June",
		income: 1390.45,
		expenses: 890.35
	},
	{ 	
		id: 7,
		month: "July",
		income: 454.89,
		expenses: 567.35
	},
	{ 	
		id: 8,
		month: "August",
		income: 1400.99,
		expenses: 980.25
	},
	{ 	
		id: 9,
		month: "September",
		income: 560.45,
		expenses: 345.35
	},
	{ 	
		id: 10,
		month: "October",
		income: 1045.45,
		expenses: 1200.35
	},
	{ 	
		id: 11,
		month: "November",
		income: 789.45,
		expenses: 467.35
	},
	{ 	
		id: 12,
		month: "December",
		income: 1045.45,
		expenses: 560.35
	}
];