import React, { Component } from 'react';
import FeedView from './containers/FeedView/FeedView';
import StatisticsView from './containers/StatisticsView/StatisticsView'; 
import { dataForLocalStorage } from './dataForLocalStorage';

class App extends Component {
  constructor() {
    super();
    this.state = {
    	route: 'statisticsview'
    };
  }

componentDidMount() {
     if (!(localStorage.hasOwnProperty("scandi-data"))) {
      localStorage.setItem("scandi-data", JSON.stringify(dataForLocalStorage));
     }
};

onRouteChange = (route) => {
    this.setState({ route: route });    
}

updateLocalStorage = (monthId, key, value) => {
  let dataFromStorage = JSON.parse(localStorage.getItem("scandi-data"));

  dataFromStorage.forEach(item => {
  	if (item.id === monthId) {
  		item[key] = value;
  	}
  });

  if (monthId < 1 || monthId > 12) {
      console.log(`please provide valid "monthId" param - between 1 and 12.`);
  }

  if (key !== "income" && "expenses" ) {
  		console.log(`please provide valid "key" param - "income" or "expenses"`);
  }

  if (isNaN(value)) {
	  	console.log(`please provide numeric input for "value" param!`);
  }

  localStorage.setItem("scandi-data", JSON.stringify(dataFromStorage)); 
}
 

  render() {
	  return (
	    <div>
	        { (this.state.route === 'feedview')
	          ? <div>
	              <FeedView  onRouteChange={this.onRouteChange} />
	           </div>
	          : <div>
           			<StatisticsView onRouteChange={this.onRouteChange}/> 
           		</div>           
	        }
	    </div>
	  );
  }
}

export default App; 
