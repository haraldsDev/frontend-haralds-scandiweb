import React, { Component } from 'react';
import './StatisticsView.css';
import arrowBack from '../icons/arrowBack.svg';
import statistics1 from '../icons/statistics1.svg';
import restaurant from '../icons/restaurant.svg';
import trending_up from '../icons/trending_up.svg';
import trending_down from '../icons/trending_down.svg';

class StatisticsView extends Component { 
	constructor() {
    super();
    this.state = {
    	activeMonth: 8,
    	monthPanelPosition: -375,
    	activeIncome: 1400.99,
    	activeExpenses: 980.25,
    	activeBalance: 420.74
    };
  }

updateActiveMonth = (number) => {
	this.setState({ activeMonth: number }, () => {
		const offset = 150;
		let calculationForMonthPosition = offset - ((this.state.activeMonth-1) * 75);
		this.setState({monthPanelPosition: calculationForMonthPosition}, () => {
			this.updateDisplayedBalance();
		});
	});
}

updateDisplayedBalance = () => {
	let dataFromStorage = JSON.parse(localStorage.getItem("scandi-data"));
	this.setState(
		{ activeIncome: dataFromStorage[this.state.activeMonth-1].income }, () => {
		this.setState({ activeExpenses: dataFromStorage[this.state.activeMonth-1].expenses }, () => {
			this.setState({ activeBalance: this.state.activeIncome - this.state.activeExpenses}, () => {
					console.log(this.state);		
			})
		})
	})
}

render() {
	return (
		<div className="wrapping-div white">
			<div className="top-line mt4 ml3" 
				onClick={() => this.props.onRouteChange('feedview')}>
				<div className="back-arrow-circle ba bw1 b--white-10"> 
					<img src={arrowBack} alt="" className="pointer shp9"/>
				</div>
			</div>
			<div className="statistics-panel flex justify-center">
				<div className="txt8 statistic-text">Statistic</div>
				<div className="" > 
					<img src={statistics1} alt="" className="statistics1 active"/>
				</div>
			</div> 

			<div className="months-panel flex" style={{marginLeft: this.state.monthPanelPosition}}>
				<div className="month-box" onClick={() => this.updateActiveMonth(1)}>January</div>
				<div className="month-box" onClick={() => this.updateActiveMonth(2)}>February</div>
				<div className="month-box" onClick={() => this.updateActiveMonth(3)}>March</div>
				<div className="month-box" onClick={() => this.updateActiveMonth(4)}>April</div>
				<div className="month-box" onClick={() => this.updateActiveMonth(5)}>May</div>
				<div className="month-box" onClick={() => this.updateActiveMonth(6)}>June</div>
				<div className="month-box" onClick={() => this.updateActiveMonth(7)}>July</div>
				<div className="month-box" onClick={() => this.updateActiveMonth(8)}>August</div>
				<div className="month-box" onClick={() => this.updateActiveMonth(9)}>September</div>
				<div className="month-box" onClick={() => this.updateActiveMonth(10)}>October</div>
				<div className="month-box" onClick={() => this.updateActiveMonth(11)}>November</div>
				<div className="month-box" onClick={() => this.updateActiveMonth(12)}>December</div>
			</div>
			<div className="blue-line shp6 center"></div>
			
			<div className="circle-chart">
				<div className="blue-segment-background"></div>
				<div className="blue-segment pointer dim">
					<div className="two-white-points">
						<div className="white-point-hollow"></div>
						<div className="white-point-inner"></div>
					</div>
				</div>
				<div className="sector dark-black"></div>
				<div className="sector-purple-background"></div>
				<div className="sector purple pointer dim">
					<div className="two-white-points">
						<div className="white-point-hollow"></div>
						<div className="white-point-inner"></div>
					</div>
				</div>
				<div className="sector-yellow-background"></div>
				<div className="sector yellow pointer dim">
					<div className="white-point-full"></div>
				</div>
				<div className="black-circle-middle">
					<div className="dollars-and-balance">
						<div className="dollars-and-centrs-center flex flex-row justify-center">
							<div className="dollars-center txt8">{`$${Math.floor(this.state.activeBalance)}`}</div>
							<div className="txt3 cents-center">{`${(this.state.activeBalance).toFixed(2).slice(-2)}`}</div>	
						</div>
						<div className="balance-center txt17 flex justify-center">balance</div>
					</div>
				</div>
			</div>

			<div className="food-and-restaurants mt4">
				<div className=" flex justify-center"> 
					<img src={restaurant} alt="" className=" fork-icon pointer shp9"/>
					<div className="txt8 text-230">$230</div>
				</div>
				<div className="food-and-restaurants-text flex justify-center txt7"> food and restaurants</div>
			</div>

			<div className="balance-boxes flex justify-center">
				<div className="box left-box shp1 mr2">
					<div className="dollars-container flex justify-around "> 
						<div className="txt4 dollars flex flex-row">
							<div className="dollar-text">
								{`+$${Math.floor(this.state.activeIncome)}`}
							</div>
						</div>

						<div className="cents justify-center">	
							<div className="cents-and-arrow">
								<div className="txt3 cents-number">{`${(this.state.activeIncome).toString().slice(-2)}`}</div>	
								<img src={trending_up} alt="" className="blue-cents-line"/>					
							</div>
						</div>
					</div>
				</div>
				<div className="box right-box shp1 ml2">
					<div className="dollars-container flex justify-around "> 
						<div className="txt4 dollars flex flex-row">
							<div className="dollar-text">
							{`-$${Math.floor(this.state.activeExpenses)}`}
							</div>
						</div>
						<div className="cents justify-center">	
							<div className="cents-and-arrow cents-and-arrow-right">
								<div className="txt3 cents-number">{`${(this.state.activeExpenses).toString().slice(-2)}`}</div>	
								<img src={trending_down} alt="" className="blue-cents-line"/>	
							</div>				
						</div>
					</div>
				</div> 
			</div>
		</div>
	);
  	}
}

export default StatisticsView;
	 