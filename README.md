This is a FRONTEND test project for Scandiweb by Haralds Matulis.


## Current state of the project

All finished.

Views made for FeedView and StatisticsView.

Functionality made for both Views.

# Test Description from Scandiweb

# Frontend Test

### Intro

We want you to create a development ready environment and that will serve a small application that will have initial data saved in localstorage that are managable through public API.

### Requirements

All your codebase should be available in git repository with a history of your work progress.

#### Build system

1. Create a development ready build system
2. Your build tool should compile SCSS to CSS
3. …should transform ES6 syntax
4. …should create sourcemaps for JS/SCSS
5. …should optimise assets
6. …should be watching for a code changes and compile on save
7. …should create a localhost environment

#### Application

1. Create application using provided PSD file
2. You are allowed to use vanilla JavaScript or any framework of your choice
3. Using your imagination create animations for various effects
4. Statistics data should be stored in localStorage
5. User should be able to check statistic for various months within the current year
6. Application should have public methods to manage (create/delete) data entries
6. You can extend the application as many functionality/layouts as you want

### Final words

Do as much as you can with the provided assets/information, use your imagination to create additional cool animations, interesting features, whatever you want. Don’t be afraid to send partial result if you are struggling with some step, describe issues you had and we will take it into consideration. In the of the day, your mindset and thoughts behind the implementation is what important.

Thank you and good luck!
